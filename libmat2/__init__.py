#!/bin/env python3

# A set of extension that aren't supported, despite matching a supported mimetype
unsupported_extensions = {
    '.asc',
    '.bat',
    '.brf',
    '.c',
    '.h',
    '.ksh',
    '.pl',
    '.pot',
    '.rdf',
    '.srt',
    '.text',
    '.txt',
    '.wsdl',
    '.xpdl',
    '.xsd',
    '.xsl',
    }
